const { computePoolAddress } = require('@uniswap/v3-sdk')
const { ethers } = require('ethers')
const ERC20ABI = require("../ABIs/abi.json");
const uniswapV3FactoryAddress = '0x1F98431c8aD98523631AE4a59f267346ea31F984'
const { abi: IUniswapV3PoolABI } = require('@uniswap/v3-core/artifacts/contracts/interfaces/IUniswapV3Pool.sol/IUniswapV3Pool.json')
const {FeeAmount} = require("@uniswap/v3-sdk");

exports.getPoolImmutables = async (initTokens, initProvider) => {
    let initialFee = initTokens.poolFee
    let triedFees = []

    let getPoolContract = async (tokens, provider) => {
        //====================== POOL =============================
        const poolAddress = await getPoolAddress(tokens)
        console.log('pool Address is ', poolAddress)
        const poolContract = new ethers.Contract(
            poolAddress,
            IUniswapV3PoolABI,
            provider
        )
        return poolContract
        //====================== POOL ==============================
    }
    let getPoolAddress = async (tokens) => {
        try {
            const poolAddress = computePoolAddress({
                factoryAddress: uniswapV3FactoryAddress,
                tokenA: tokens.in,
                tokenB: tokens.out,
                fee: tokens.poolFee,
            })
            return poolAddress
        } catch (e) {
            console.log('ERROR in getPoolAddress ')
            console.log(e)
        }
    }

    let tryGetPoolImmutables = async (tokens, provider) => {
        try {
            console.log('TRYING TO GET POOL CONTRACT WITH FEE = ', tokens.poolFee)
            triedFees.push(tokens.poolFee)
            let poolContract = await getPoolContract(tokens, provider)
            const [token0, token1, fee] = await Promise.all([
                poolContract.token0(),
                poolContract.token1(),
                poolContract.fee(),
            ])
            const immutables = {
                token0,
                token1,
                fee,
            }
            return immutables
        } catch (e) {
            console.log('ERROR in tryGetPoolImmutables')
            console.log(e)
            console.log(e.code)
            if (e.code === 'BAD_DATA') {
                return await retry()
            }
        }
    }

    let retry = async () => {
        let newTokens = initTokens
        if (!triedFees.includes(FeeAmount.LOWEST)) {
            newTokens.poolFee = FeeAmount.LOWEST
            return await tryGetPoolImmutables(initTokens, initProvider)
        } else if (!triedFees.includes(FeeAmount.LOW)) {
            newTokens.poolFee = FeeAmount.LOW
            return await tryGetPoolImmutables(initTokens, initProvider)
        } else if (!triedFees.includes(FeeAmount.MEDIUM)) {
            newTokens.poolFee = FeeAmount.MEDIUM
            return await tryGetPoolImmutables(initTokens, initProvider)
        } else if (!triedFees.includes(FeeAmount.HIGH)) {
            newTokens.poolFee = FeeAmount.HIGH
            return await tryGetPoolImmutables(initTokens, initProvider)
        }
    }

    try {
        return await tryGetPoolImmutables(initTokens, initProvider)
    } catch (e) {
        console.log('ERROR in getPoolImmutables')
        console.log(e)
    }
}

exports.getWalletBalances = async (provider, address, tokenFromContract, tokenToContract) => {
    // Find the balance of tokens before
    try {
        const getTokenFromBalance = await tokenFromContract.balanceOf(address)
        const getTokenToBalance = await tokenToContract.balanceOf(address)
        console.log('Balances: ')
        console.log('TokenFrom: ', ethers.formatEther(getTokenFromBalance))
        console.log('TokenTo: ', ethers.formatEther(getTokenToBalance))
    } catch (e) {
        console.log('ERROR in getWalletBalances')
        console.log(e)
    }
}

exports.getArgs = async () => {
    try {
        const args = {};
        process.argv
            .slice(2, process.argv.length)
            .forEach( arg => {
                // long arg
                if (arg.slice(0,2) === '--') {
                    const longArg = arg.split('=');
                    const longArgFlag = longArg[0].slice(2,longArg[0].length);
                    const longArgValue = longArg.length > 1 ? longArg[1] : true;
                    args[longArgFlag] = longArgValue;
                }
                // flags
                else if (arg[0] === '-') {
                    const flags = arg.slice(1,arg.length).split('');
                    flags.forEach(flag => {
                        args[flag] = true;
                    });
                }
            });
        return args;
    } catch (e) {
        console.log('ERROR in getArgs')
        console.log(e)
    }
}

exports.checkAllowance = async (tokenContract, spender, wallet, approveAmount) => {
    try {
        const currentAllowance = await tokenContract.allowance(wallet.address, spender);
        if (currentAllowance < approveAmount) {
            const tx = await tokenContract.connect(wallet).approve(spender, approveAmount);
            console.log(`Approve transaction hash: ${tx.hash}`);
            await tx.wait();
            console.log(`Approved ${approveAmount} tokens to ${spender}`);
        } else {
            console.log("No need to approve - sufficient allowance already exists");
        }
    } catch (e) {
        console.log('ERROR in checkAllowance')
        console.log(e)
    }
};

exports.checkBalanceForGas = async (provider, userWallet) => {
    try {
        const balance = await provider.getBalance(userWallet.address);
        console.log('CHECKING BALANCE FOR GAS: ', balance)
        const balanceInEther = parseFloat(ethers.formatEther(balance));
        console.log('BALANCE IN ETH: ', balanceInEther)
        return balanceInEther
    } catch (e) {
        console.log('ERROR in checkBalanceForGas')
        console.log(e)
    }
}

exports.sleep = async (milliseconds) => {
    const date = Date.now();
    let currentDate = null;
    do {
        currentDate = Date.now();
    } while (currentDate - date < milliseconds);
}

exports.encodePath = async (path, fees) => {
    try {
        let FEE_SIZE = 3
        if (path.length != fees.length + 1) {
            throw new Error('path/fee lengths do not match')
        }
        let encoded = '0x'
        for (let i = 0; i < fees.length; i++) {
            // 20 byte encoding of the address
            encoded += path[i].slice(2)
            // 3 byte encoding of the fee
            encoded += fees[i].toString(16).padStart(2 * FEE_SIZE, '0')
        }
        // encode the final token
        encoded += path[path.length - 1].slice(2)

        return encoded.toLowerCase()
    } catch (e) {
        console.log('ERROR in encodePath')
        console.log(e)
    }
}

// exports.approveTransfer = async (amountIn, userWallet) => {
//     console.log('Approving transfer....')
//     console.log('----------------------------------------')
//
//     try {
//         //const approvalAmount = (amountIn * 100000).toString()
//         const approvalAmount = amountIn
//
//         console.log('approvalAmount ', approvalAmount)
//
//         const tokenFromContract = new ethers.Contract(
//             TokenFrom.address,
//             ERC20ABI,
//             provider
//         )
//
//         const balance = await provider.getBalance(userWallet.address);
//         console.log(balance)
//         const balanceInEther = parseFloat(ethers.formatEther(balance));
//         console.log(balanceInEther)
//
//         let res = await tokenFromContract.connect(userWallet).approve(swapRouterAddress, approvalAmount)
//
//         console.log(res);
//
//         console.log('Approve Transfer Success! ')
//     } catch (e) {
//         console.log('ERROR in approveTransfer')
//         console.log(e)
//     }
// }