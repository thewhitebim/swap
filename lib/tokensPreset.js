const tokensPreset = {
    //GOERLI
    5: {
        name: 'WETH',
        address: '0xB4FBF271143F4FBf7B91A5ded31805e42b2208d6'
    },
    //POLYGON MUMBAI
    80001: {
        name: 'WMATIC',
        address: '0x9c3C9283D3e44854697Cd22D3Faa240Cfb032889'
    },
    //POLYGON
    137: {
        name: 'WMATIC',
        address: '0x0d500B1d8E8eF31E21C99d1Db9A6444d3ADf1270'
    }
}
module.exports = {
    tokensPreset
}