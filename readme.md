# UniSwap universal swapper
## how to:

```sh
node uniswapTrader --chain_id=CHAIN_ID --token_in=TOKEN_IN_CONTRACT --token_out=TOKEN_OUT_CONTRACT --amount_in=AMOUNT --slippage=PERCENT --wallet_private_key=YOUR_PRIVATE_KEY
```
if ```--token_in``` or ```--token_out``` are not passed, token you swap from/to will be native to the chain. 

Example of NATIVE ETH for UNI (NO TOKEN IN!!!): 
```sh
node uniswapTrader --chain_id=5 --token_out=0x1f9840a85d5aF5bf1D1762F925BDADdC4201F984 --amount_in=0.001 --slippage=0.5 --wallet_private_key=YOUR_PRIVATE_KEY
```