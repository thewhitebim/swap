const { ethers } = require('ethers')
const {abi: QUOTER_ABI } = require('@uniswap/v3-periphery/artifacts/contracts/interfaces/IQuoter.sol/IQuoter.json')
const {getPoolImmutables,
  getPoolAddress,
  getWalletBalances,
  getArgs,
  checkAllowance,
  checkBalanceForGas,
  sleep,
  encodePath} = require('./lib/helpers')
const ERC20ABI = require('./ABIs/abi.json')
const executeABI = require('./ABIs/executeABI.json')
const {SwapRouter, FeeAmount, encodeRouteToPath} = require("@uniswap/v3-sdk");
const {extraRpcs} = require('./lib/RPCs')
const {Token} = require("@uniswap/sdk-core");
const {tokensPreset} = require("./lib/tokensPreset");
//const {abi: SwapRouterABI } = require('@uniswap/v3-periphery/artifacts/contracts/interfaces/ISwapRouter.sol/ISwapRouter.json')
//const {abi: V3SwapRoUterABI } = require('@uniswap/v3-periphery/artifacts/contracts/SwapRouter.sol/SwapRouter.json')


let WALLET_PRIVATE_KEY// = process.env.WALLET_PRIVATE_KEY
let QUOTER_ADDRESS = '0xb27308f9F90D607463bb33eA1BeBb41C27CE5AB6'

//const swapRouterAddress = '0xE592427A0AEce92De3Edee1F18E0157C05861564'
const swapRouterAddress = '0x4648a43B2C14Da09FdF82B161150d3F634f40491' // UNIVERSAL ROUTER
//const uniswapV3FactoryAddress = '0x1F98431c8aD98523631AE4a59f267346ea31F984'

let chainId = null,
    rpc = null,
    TOKEN_FROM,
    TOKEN_TO,
    tokens,
    constantsInput,
    tokenFromContract,
    tokenToContract,
    immutables,
    SWAP_FROM_NATIVE_TOKEN = false,
    SWAP_TO_NATIVE_TOKEN = false,
    AMOUNT,
    provider,
    option = ethers.JsonRpcApiProviderOptions = {
      batchMaxCount: 1
    };

const TokenFrom = {
  name: '',
  symbol: '',
  decimals: 18,
  address: '',
}
const TokenTo = {
  name: '',
  symbol: '',
  decimals: 18,
  address: '',
}

async function MAIN() {
  constantsInput = await getArgs()
  console.log(constantsInput)

  let rpcObj = extraRpcs[Number(constantsInput.chain_id)].rpcs
  rpc = rpcObj[0].url? rpcObj[0].url: rpcObj[0]
  console.log('RPC URL IS ', rpc)

  provider = new ethers.JsonRpcProvider(rpc, Number(constantsInput.chain_id), option);

  await setTokensData()

  const wallet = new ethers.Wallet(WALLET_PRIVATE_KEY, provider)
  const connectedWallet = wallet.connect(provider)

  let balanceInEther = await checkBalanceForGas(provider, wallet)
  const tokenFromBalance = Number(ethers.formatEther(await tokenFromContract.balanceOf(wallet.getAddress())))
  console.log('TOKEN FROM BALANCE: ', tokenFromBalance)


  if ((!SWAP_FROM_NATIVE_TOKEN && tokenFromBalance >= AMOUNT) ||
      (SWAP_FROM_NATIVE_TOKEN && AMOUNT <= balanceInEther)) {

    console.log('AMOUNT IN = ', AMOUNT)
    const amountIn = ethers.parseUnits(AMOUNT.toString(), TokenFrom.decimals)
    console.log('amountIn is ', amountIn)

    immutables = await getPoolImmutables(tokens, provider)
    console.log('immutables')
    console.log(immutables)

    const swapRouterContract = new ethers.Contract(
        swapRouterAddress,
        executeABI,
        //V3SwapRoUterABI,
        //SwapRouterABI,
        provider
    )

    //tokenContract, spender, wallet, approveAmount
    await checkAllowance(tokenFromContract, swapRouterAddress, connectedWallet, amountIn)

    console.log('Wallet balances before:')
    await getWalletBalances(provider, wallet.getAddress(), tokenFromContract, tokenToContract)
    console.log('====================================')

    //========================== EXECUTE TRANSACTION ==============================

    let tokensForPath = [immutables.token0, immutables.token1]
    if (SWAP_TO_NATIVE_TOKEN === true) {
      tokensForPath = [immutables.token0, immutables.token1]
    } else if (SWAP_FROM_NATIVE_TOKEN === true) {
      tokensForPath = [immutables.token1, immutables.token0]
    }

    let encodedPathForExecuteTransaction  = await encodePath(tokensForPath, [immutables.fee])
    console.log('encodedPathForExecuteTransaction: ', encodedPathForExecuteTransaction)

    // GETTING THE QUOTE
    const Quoter = await new ethers.Contract(QUOTER_ADDRESS, QUOTER_ABI, provider)
    console.log('============got Quoter===========')
    console.log(Quoter)

    const quote = await Quoter.quoteExactInput.staticCall(
        encodedPathForExecuteTransaction,
        amountIn);
    console.log('=============== THE QUOTE IS ================ ', quote)
    // GETTING THE QUOTE

    const abiCoder = new ethers.AbiCoder();

//============================ INPUT 1 ============================
    let encodedFirstInputForExecuteTransaction = abiCoder.encode(// recipient, input, minOut, path, fromOwner
        ["address", "uint256", "uint256", "bytes", "bool"],
        ['0x0000000000000000000000000000000000000001',
          amountIn,
          quote,
          encodedPathForExecuteTransaction,
          true]
    )

    if (SWAP_FROM_NATIVE_TOKEN === true) {
      encodedFirstInputForExecuteTransaction = abiCoder.encode(//address, amount
          ["address", "uint256"],
          ['0x0000000000000000000000000000000000000002', amountIn]
      )
    } else if (SWAP_TO_NATIVE_TOKEN === true) {
      encodedFirstInputForExecuteTransaction = abiCoder.encode(// recipient, input, minOut, path, fromOwner
          ["address", "uint256", "uint256", "bytes", "bool"],
          ['0x0000000000000000000000000000000000000002',
            amountIn,
            quote,
            encodedPathForExecuteTransaction,
            true]
      )
    }
    console.log('encodedFirstInputForExecuteTransaction')
    console.log(encodedFirstInputForExecuteTransaction)
//============================ INPUT 1 ============================

//============================ INPUT 2 ============================
    let encodedSecondInputForExecuteTransaction = null

    if (SWAP_TO_NATIVE_TOKEN === true) {
      encodedSecondInputForExecuteTransaction = abiCoder.encode(
          ["address", "uint256"],
          ['0x0000000000000000000000000000000000000001', quote]
      )
    } else if (SWAP_FROM_NATIVE_TOKEN === true) {
      encodedSecondInputForExecuteTransaction = abiCoder.encode(// recipient, input, minOut, path, fromOwner
          ["address", "uint256", "uint256", "bytes", "bool"],
          ['0x0000000000000000000000000000000000000001',
            amountIn,
            quote,
            encodedPathForExecuteTransaction,
            false]
      )
    }
    console.log('encodedSecondInputForExecuteTransaction')
    console.log(encodedSecondInputForExecuteTransaction)
//============================ INPUT 2 ============================

    let commands = '0x00' //V3_SWAP_EXACT_IN
    let inputs = [encodedFirstInputForExecuteTransaction]

    if (SWAP_FROM_NATIVE_TOKEN === true) {
      commands = '0x0b00' //WRAP_ETH + V3_SWAP_EXACT_IN
      inputs.push(encodedSecondInputForExecuteTransaction)
    } else if (SWAP_TO_NATIVE_TOKEN === true) {
      commands = '0x000c' //V3_SWAP_EXACT_IN + UNWRAP_WETH
      inputs.push(encodedSecondInputForExecuteTransaction)
    }

    console.log('data for EXECUTE transaction')
    console.log([
      commands,
      inputs,
      Math.floor(Date.now() / 1000) + 60 * 20
    ])

    let data = swapRouterContract.interface.encodeFunctionData('execute', [
      commands,
      inputs,
      Math.floor(Date.now() / 1000) + 60 * 20
    ])

    const executeTransaction = {
      to: swapRouterAddress,
      from: await wallet.getAddress(),
      //data: executeParams,
      data: data,
    };

    if (SWAP_FROM_NATIVE_TOKEN === true) {
      executeTransaction.value = amountIn
    }

    console.log('=====================THE EXECUTE TRANSACTION IS===================')
    console.log(executeTransaction)

    //ESTIMATE GAS
    const GAS = await wallet.estimateGas(executeTransaction).then(async estimatedGas => {
      console.log('============ GAS AMOUNT ===========', estimatedGas)

      let feeData = await provider.getFeeData()
      console.log('feeData is ', feeData)

      const multipliedGas = (feeData.maxFeePerGas+feeData.maxPriorityFeePerGas)*estimatedGas
      console.log('multipliedGas is ', multipliedGas)

      const multipliedGasInEther = parseFloat(ethers.formatEther(multipliedGas));
      console.log('MULTIPLIED GAS IN ETH: ', multipliedGasInEther)

      //zzzzzzzzzzzzzzzzzzzzzzzz SLEEP zzzzzzzzzzzzzzzzzzzzzzzz//
      console.log("Start sleep");
      await sleep(2000);
      console.log("End sleep!");
      //zzzzzzzzzzzzzzzzzzzzzzzz SLEEP zzzzzzzzzzzzzzzzzzzzzzzz//

      console.log('===================================================================')
      console.log('BEFORE transaction')
      console.log('SWAP_FROM_NATIVE_TOKEN: ', SWAP_FROM_NATIVE_TOKEN)
      console.log('multipliedGasInEther ', multipliedGasInEther)
      console.log('AMOUNT ', AMOUNT)
      console.log('multipliedGasInEther+AMOUNT ', Number(multipliedGasInEther)+AMOUNT)
      console.log('===================================================================')

      if ((SWAP_FROM_NATIVE_TOKEN && Number(multipliedGasInEther)+AMOUNT <= balanceInEther) ||
          (!SWAP_FROM_NATIVE_TOKEN && multipliedGasInEther <= balanceInEther)) {
        const executeTransactionResponse = await wallet.sendTransaction({
          ...executeTransaction,
          //gasPrice,
          gasLimit: estimatedGas,
          //maxPriorityFeePerGas: ethers.parseUnits("1.5", "gwei"),
          maxPriorityFeePerGas: feeData.maxPriorityFeePerGas,
          maxFeePerGas: feeData.maxFeePerGas,
          //maxFeePerGas:
          //value: 0,
        }).then(async (transaction) => {
          console.log('Wallet balances After:')
          await getWalletBalances(provider, wallet.getAddress(), tokenFromContract, tokenToContract)
          console.log('====================================')
          console.log(transaction)
        }).catch((err) => {
          console.log('Failed send transaction')
          console.log(err)
        });
        //TODO REFUND ETH
        //swapRouterContract.refundETH()
      } else {
        console.log('TRANSACTION CANCELLED: conditions have not been met')
        console.log('SWAP_FROM_NATIVE_TOKEN: ', SWAP_FROM_NATIVE_TOKEN)
        console.log('multipliedGasInEther ', multipliedGasInEther)
        console.log('AMOUNT ', AMOUNT)
        console.log('multipliedGasInEther+AMOUNT ', Number(multipliedGasInEther)+AMOUNT)
      }

    }).catch((err) => {
      console.log('FAILED ESTIMATE GAS')
      console.log(err)
    });
    //========================== EXECUTE TRANSACTION ==============================
  } else {
    console.log('BALANCE IS LOWER THAN AMOUNT IN: ', `ETH balance: ${balanceInEther}, balance: ${tokenFromBalance}, amountIn: ${AMOUNT}, swap from native: ${SWAP_FROM_NATIVE_TOKEN}`)
  }
}

const setTokensData = async () => {
  try {
    chainId = Number(constantsInput.chain_id)
    WALLET_PRIVATE_KEY = constantsInput.wallet_private_key

    let nativeToken,
        tokenIn,
        tokenOut

    if (!constantsInput.token_in || !constantsInput.token_out) {
      nativeToken = getNativeToken(chainId)
    }

    if (constantsInput.token_in) {
      tokenIn = constantsInput.token_in
    } else {
      tokenIn = nativeToken
      SWAP_FROM_NATIVE_TOKEN = true
    }

    if (constantsInput.token_out) {
      tokenOut = constantsInput.token_out
    } else {
      tokenOut = nativeToken
      SWAP_TO_NATIVE_TOKEN = true
    }

    TokenFrom.address = tokenIn
    tokenFromContract = new ethers.Contract(
        TokenFrom.address,
        ERC20ABI,
        //MATIC_ABI,
        provider
    )
    TokenFrom.symbol = await tokenFromContract.symbol()
    TokenFrom.name = await tokenFromContract.name()
    TokenFrom.decimals = await tokenFromContract.decimals()

    TOKEN_FROM = new Token(
        chainId,
        TokenFrom.address,
        Number(TokenFrom.decimals),
        TokenFrom.symbol,
        TokenFrom.name
    )


    TokenTo.address = tokenOut
    tokenToContract = new ethers.Contract(
        TokenTo.address,
        ERC20ABI,
        //ZETA_ABI,
        provider
    )
    TokenTo.symbol = await tokenToContract.symbol()
    TokenTo.name = await tokenToContract.name()
    TokenTo.decimals = await tokenToContract.decimals()

    TOKEN_TO = new Token(
        chainId,
        TokenTo.address,
        Number(TokenTo.decimals),
        TokenTo.symbol,
        TokenTo.name
    )

    AMOUNT = Number(constantsInput.amount_in)

    tokens = {
      in: TOKEN_FROM,
      amountIn: constantsInput.amount_in,
      out: TOKEN_TO,
      poolFee: FeeAmount.MEDIUM
    }

    console.log('tokens')
    console.log(tokens)

    // immutables = {
    //   token0: TOKEN_TO.address,
    //   token1: TOKEN_FROM.address,
    //   fee: FeeAmount.MEDIUM
    // }
  } catch (e) {
    console.log('error in setTokensData')
    console.log(e)
    throw e
  }
}

const getNativeToken = (chainId) => {
  return tokensPreset[chainId].address
}

MAIN()
