const { ethers } = require('ethers')
const { abi: IUniswapV3PoolABI } = require('@uniswap/v3-core/artifacts/contracts/interfaces/IUniswapV3Pool.sol/IUniswapV3Pool.json')
const {abi: SwapRouterABI } = require('@uniswap/v3-periphery/artifacts/contracts/interfaces/ISwapRouter.sol/ISwapRouter.json')
const {abi: QUOTER_ABI } = require('@uniswap/v3-periphery/artifacts/contracts/interfaces/IQuoter.sol/IQuoter.json')
const {abi: V3SwapRoUterABI } = require('@uniswap/v3-periphery/artifacts/contracts/SwapRouter.sol/SwapRouter.json')
const {getPoolImmutables, getPoolAddress, getWalletBalances, getArgs, checkAllowance, checkBalanceForGas} = require('./lib/helpers')
//const { swapRouterAddress, TokenFrom, TokenTo, chainId, UNISWAP_TOKEN, GOERLI_ETH} = require('./lib/constants')
const ERC20ABI = require('./ABIs/abi.json')
// const ZETA_ABI = require('./ABIs/zetaABI.json')
// const MATIC_ABI = require('./ABIs/maticABI.json')
// const UNIABI = require('./ABIs/uniAbi.json')
const executeABI = require('./ABIs/executeABI.json')
//const QUOTER_ABI = require('./ABIs/quoter_abi.json')
const {SwapRouter, FeeAmount, encodeRouteToPath} = require("@uniswap/v3-sdk");
const { argv } = require('node:process');
const {extraRpcs} = require('./lib/RPCs')
const {Token} = require("@uniswap/sdk-core");
const { computePoolAddress } = require('@uniswap/v3-sdk')
//const {Config} = require("./lib/config");
const {tokensPreset} = require("./lib/tokensPreset");
require('dotenv').config()


const INFURA_URL_TESTNET = process.env.INFURA_URL_TESTNET
let WALLET_PRIVATE_KEY// = process.env.WALLET_PRIVATE_KEY
let QUOTER_ADDRESS = '0xb27308f9F90D607463bb33eA1BeBb41C27CE5AB6'

//const swapRouterAddress = '0xE592427A0AEce92De3Edee1F18E0157C05861564'
const swapRouterAddress = '0x4648a43B2C14Da09FdF82B161150d3F634f40491' // UNIVERSAL ROUTER
//const uniswapV3FactoryAddress = '0x1F98431c8aD98523631AE4a59f267346ea31F984'


// async function parseTx() {
//   let txHash = '0xac2da1943da2cb2f14ce551d3778fdee1dcb3eaf80f065ca04acdf4388efbf7a'
//   const ABI = [
//     'function execute(bytes commands,bytes[] inputs,uint256 deadline)'
//   ]
//
//   const provider = await new ethers.JsonRpcProvider("https://rpc.ankr.com/eth_goerli", 5);
//
//   let pendingTx = await provider.getTransaction(txHash);
//
//   console.log(pendingTx)
//
//   const iface = new ethers.Interface(ABI);
//   let decodedData = iface.parseTransaction({ data: pendingTx.data, value: pendingTx.value });
//
//   //let tx = await ethers.parseTransaction(txHash)
//
//   console.log(decodedData)
//
//   console.log(decodedData.args[1][1])
// }
//
// parseTx()

let chainId = null,
    rpc = null,
    TOKEN_FROM,
    TOKEN_TO,
    tokens,
    constantsInput,
    tokenFromContract,
    tokenToContract,
    immutables,
    swapFromNativeETH = false,
    swapToNativeETH = false,
    AMOUNT

const TokenFrom = {
    name: '',
    symbol: '',
    decimals: 18,
    address: '',
}
const TokenTo = {
    name: '',
    symbol: '',
    decimals: 18,
    address: '',
}

const args = process.argv.slice(2);

//const provider = new ethers.JsonRpcProvider(INFURA_URL_TESTNET) // Goerli
//const provider = new ethers.JsonRpcProvider("https://rpc.ankr.com/eth_goerli", 5);// Goerli
let provider// = new ethers.JsonRpcProvider(rpc, Number(constantsInput.chain_id));
let option = ethers.JsonRpcApiProviderOptions = {
    batchMaxCount: 1
};

async function main() {
    constantsInput = await getArgs()
    console.log(constantsInput)

    let rpcObj = extraRpcs[Number(constantsInput.chain_id)].rpcs
    rpc = rpcObj[0].url? rpcObj[0].url: rpcObj[0]
    console.log('RPC URL IS ', rpc)

    provider = new ethers.JsonRpcProvider(rpc, Number(constantsInput.chain_id), option);

    await setTokensData()

    //const wallet = ethers.Wallet.fromPhrase(WALLET_SECRET)
    const wallet = new ethers.Wallet(WALLET_PRIVATE_KEY, provider)
    const connectedWallet = wallet.connect(provider)
    let balanceInEther = await checkBalanceForGas(provider, wallet)
    const tokenFromBalance = Number(ethers.formatEther(await tokenFromContract.balanceOf(wallet.getAddress())))
    console.log('TOKEN FROM BALANCE: ', tokenFromBalance)
    if ((!swapFromNativeETH && tokenFromBalance >= AMOUNT) ||
        (swapFromNativeETH &&  AMOUNT <= balanceInEther)) {

        console.log('AMOUNT IN = ', AMOUNT)

        const amountIn = ethers.parseUnits(AMOUNT.toString(), TokenFrom.decimals)

        console.log('amountIn is ', amountIn)

        //================ POOL ======================
        const poolAddress = await getPoolAddress(tokens)
        console.log('pool Address is ', poolAddress)

        const poolContract = new ethers.Contract(
            poolAddress,
            IUniswapV3PoolABI,
            provider
        )

        console.log('poolContract')
        console.log(poolContract)
        //console.log('poolContract.fee()')
        //console.log(poolContract.fee())
        //================ POOL ======================

        immutables = await getPoolImmutables(poolContract)

        console.log('immutables')
        console.log(immutables)


        const swapRouterContract = new ethers.Contract(
            swapRouterAddress,
            executeABI,
            //V3SwapRoUterABI,
            //SwapRouterABI,
            provider
        )

        //await approveTransfer(amountIn, connectedWallet)

        //tokenContract, spender, wallet, approveAmount
        await checkAllowance(tokenFromContract, swapRouterAddress, connectedWallet, amountIn)

        console.log('Wallet balances before:')
        await getWalletBalances(provider, wallet.getAddress(), tokenFromContract, tokenToContract)

        console.log('====================================')

        //let gasLimit = 1600000
        //gasLimit = ethers.hexlify(ethers.encodeBytes32String('1000000'))

        /*const transaction = await swapRouterContract
            .connect(connectedWallet)
            .exactInputSingle(params, {
              gasLimit: gasLimit ,
            })
            .then(async (transaction) => {
              console.log('Wallet balances After:')
              await getWalletBalances(provider, wallet.getAddress())

              console.log('====================================')

              console.log(transaction)
            })
            .catch((err) => {
              console.log('Failed send transaction')
              console.log(err)
            })
      */


        const abiCoder = new ethers.AbiCoder();

// //========================ENCODING INPUTS =====================
//
        //let decodedFirstInputOriginal = abiCoder.decode(["address", "uint256"], '0x00000000000000000000000000000000000000000000000000000000000000010000000000000000000000000000000000000000000000000003703878445024')
        //let decodedFirstInputCustom = abiCoder.decode(["address", "uint256"], '0x000000000000000000000000000000000000000000000000000000000000000100000000000000000000000000000000000000000000000000037482d0a7409a')
//
        // console.log('====================================================================')
        // console.log('DECODED ORIGINAL INPUT 1')
        // console.log(decodedFirstInputOriginal)
        // console.log('amount = ', ethers.formatEther(decodedFirstInputOriginal[1]))
        // console.log('DECODED CUSTOM INPUT 1')
        // console.log(decodedFirstInputCustom)
        // console.log('amount = ', ethers.formatEther(decodedFirstInputCustom[1]))
        // console.log('====================================================================')
//
//   // ['0x0000000000000000000000000000000000000002', 1000000000000000n ]
//   let encodedFirstInput = abiCoder.encode(["address", "uint256"],
//       [
//           '0x0000000000000000000000000000000000000002',
//         1000000000000000n
//       ])
//   //console.log('encodedFirstInput')
//   //console.log(encodedFirstInput)
//
//   let secondInput = '0x000000000000000000000000000000000000000000000000000000000000000100000000000000000000000000000000000000000000000000038d7ea4c6800000000000000000000000000000000000000000000000000000a2cd4cbc83268c00000000000000000000000000000000000000000000000000000000000000a00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000002bb4fbf271143f4fbf7b91a5ded31805e42b2208d60027101f9840a85d5af5bf1d1762f925bdaddc4201f984000000000000000000000000000000000000000000'
//   let secondInput2 = '0x000000000000000000000000000000000000000000000000000000000000000100000000000000000000000000000000000000000000000000038d7ea4c6800000000000000000000000000000000000000000000000000000a901ccfbd7f6d600000000000000000000000000000000000000000000000000000000000000a00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000002bb4fbf271143f4fbf7b91a5ded31805e42b2208d60027101f9840a85d5af5bf1d1762f925bdaddc4201f984000000000000000000000000000000000000000000'
//   let decodedSecondInput = abiCoder.decode(["address", "uint256", "uint256", "bytes", "bool"], secondInput);
        //let decodedSecondInputOriginal = abiCoder.decode(["address", "uint256", "uint256", "bytes", "bool"], '0x0000000000000000000000000000000000000000000000000000000000000001000000000000000000000000000000000000000000000000016345785d8a0000000000000000000000000000000000000000000000000000000b84b0837c20a100000000000000000000000000000000000000000000000000000000000000a00000000000000000000000000000000000000000000000000000000000000001000000000000000000000000000000000000000000000000000000000000002b1f9840a85d5af5bf1d1762f925bdaddc4201f984000bb8b4fbf271143f4fbf7b91a5ded31805e42b2208d6000000000000000000000000000000000000000000');
        //let decodedSecondInputCustom = abiCoder.decode(["address", "uint256", "uint256", "bytes", "bool"], '0x0000000000000000000000000000000000000000000000000000000000000001000000000000000000000000000000000000000000000000016345785d8a0000000000000000000000000000000000000000000000000000000b8eea5d834e4800000000000000000000000000000000000000000000000000000000000000a00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000002b1f9840a85d5af5bf1d1762f925bdaddc4201f9840001f4b4fbf271143f4fbf7b91a5ded31805e42b2208d6000000000000000000000000000000000000000000');
//   //console.log('THE DECODED SECOND INPUT IS (recipient, input, minOut, path, fromOwner)')
//   //console.log(decodedSecondInput)
//   // Result(5) [
//   //     '0x0000000000000000000000000000000000000001',
//   //         1000000000000000n,
//   //         45824675691046540n,
//   //         '0xb4fbf271143f4fbf7b91a5ded31805e42b2208d60027101f9840a85d5af5bf1d1762f925bdaddc4201f984',
//   //         false
//   //     ]
//   //     [
//   //     '0xb4fbf271143f4fbf7b91a5ded31805e42b2208d6',
//   //         '0x1f9840a85d5af5bf1d1762f925bdaddc4201f984'
//   //     ]
// //002710 fee  === 10000
//
        // console.log('====================================================================')
        // console.log('DECODED ORIGINAL INPUT 2')
        // console.log(decodedSecondInputOriginal)
        // console.log('min out = ', ethers.formatEther(decodedSecondInputOriginal[2]))
        // console.log('DECODED CUSTOM INPUT 2')
        // console.log(decodedSecondInputCustom)
        // console.log('min out = ', ethers.formatEther(decodedSecondInputCustom[2]))
        // console.log('====================================================================')
//   //========================ENCODING INPUTS =====================

//   // =================== EXTRACT PATH FROM V3 ==================
//   function extractPathFromV3(fullPath, reverse = false) {
//     const fullPathWithoutHexSymbol = fullPath.substring(2);
//     let path = [];
//     let currentAddress = "";
//     for (let i = 0; i < fullPathWithoutHexSymbol.length; i++) {
//       currentAddress += fullPathWithoutHexSymbol[i];
//       if (currentAddress.length === 40) {
//         path.push('0x' + currentAddress);
//         i = i + 6;
//         currentAddress = "";
//       }
//     }
//     if (reverse) {
//       return path.reverse();
//     }
//     return path;
//   }
//
//   let thePath = extractPathFromV3(decoded[3])
//
//   console.log('thePath')
//   console.log(thePath)
// // =================== EXTRACT PATH FROM V3 ==================


        //========================== EXECUTE TRANSACTION ==============================
        let FEE_SIZE = 3
        async function encodePath(path, fees) {
            if (path.length != fees.length + 1) {
                throw new Error('path/fee lengths do not match')
            }

            let encoded = '0x'
            for (let i = 0; i < fees.length; i++) {
                // 20 byte encoding of the address
                encoded += path[i].slice(2)
                // 3 byte encoding of the fee
                encoded += fees[i].toString(16).padStart(2 * FEE_SIZE, '0')
            }
            // encode the final token
            encoded += path[path.length - 1].slice(2)

            return encoded.toLowerCase()
        }

        // async function encodePath(path, fees) {
        //   if (path.length != fees.length + 1) {
        //     throw new Error('path/fee lengths do not match')
        //   }
        //
        //
        //   let encoded = '0x'
        //   for (let i = 0; i < fees.length; i++) {
        //     // 20 byte encoding of the address
        //     encoded += String(path[i]).slice(2)
        //     // 3 byte encoding of the fee
        //     encoded += fees[i]
        //   }
        //   // encode the final token
        //   encoded += path[path.length - 1].slice(2)
        //
        //   return encoded.toLowerCase()
        // }
        let tokensForPath = [immutables.token0, immutables.token1]
        if (swapToNativeETH === true) {
            tokensForPath = [immutables.token0, immutables.token1]
        } else if (swapFromNativeETH === true) {
            tokensForPath = [immutables.token1, immutables.token0]
        }

        let encodedPathForExecuteTransaction  = await encodePath(tokensForPath, [immutables.fee])

        //let encodedPathForExecuteTransaction = await encodeRouteToPath(path);

        // console.log('encodedPath WETH to UNI must be')
        // console.log('0xb4fbf271143f4fbf7b91a5ded31805e42b2208d60027101f9840a85d5af5bf1d1762f925bdaddc4201f984')
        console.log('encodedPathForExecuteTransaction: ', encodedPathForExecuteTransaction)


        // GETTING THE QUOTE
        const Quoter  = await new ethers.Contract(QUOTER_ADDRESS, QUOTER_ABI, provider)
        console.log('============got Quoter===========')
        console.log(Quoter)
        //console.log(Quoter)
        // const quote = await Quoter.quoteExactInputSingle.staticCall(
        //     immutables.token1,
        //     immutables.token0,
        //     immutables.fee,
        //     amountIn,
        //     0);
        const quote = await Quoter.quoteExactInput.staticCall(
            encodedPathForExecuteTransaction,
            amountIn);
        console.log('=============== THE QUOTE IS ================ ', quote)
        // GETTING THE QUOTE



//TODO должен ли быть адрес 2 если swapToNativeETH ???
        let encodedFirstInputForExecuteTransaction = abiCoder.encode(// recipient, input, minOut, path, fromOwner
            ["address", "uint256", "uint256", "bytes", "bool"],
            ['0x0000000000000000000000000000000000000001',
                amountIn,
                quote,
                encodedPathForExecuteTransaction,
                true]
        )
        if (swapFromNativeETH === true) {
            encodedFirstInputForExecuteTransaction = abiCoder.encode(//address, amount
                ["address", "uint256"],
                ['0x0000000000000000000000000000000000000002', amountIn]
            )
        } else if (swapToNativeETH === true) {
            encodedFirstInputForExecuteTransaction = abiCoder.encode(// recipient, input, minOut, path, fromOwner
                ["address", "uint256", "uint256", "bytes", "bool"],
                ['0x0000000000000000000000000000000000000002',
                    amountIn,
                    //3139143382866588n,
                    quote,
                    encodedPathForExecuteTransaction,
                    true]
            )
        }
        console.log('encodedFirstInputForExecuteTransaction')
        console.log(encodedFirstInputForExecuteTransaction)


        let encodedSecondInputForExecuteTransaction = null

        if (swapToNativeETH === true) {
            encodedSecondInputForExecuteTransaction = abiCoder.encode(
                ["address", "uint256"],
                ['0x0000000000000000000000000000000000000001', quote/*3139143382866588n*/]
            )
        } else if (swapFromNativeETH === true) {
            encodedSecondInputForExecuteTransaction = abiCoder.encode(// recipient, input, minOut, path, fromOwner
                ["address", "uint256", "uint256", "bytes", "bool"],
                ['0x0000000000000000000000000000000000000001',
                    amountIn,
                    quote,
                    encodedPathForExecuteTransaction,
                    false]
            )
        }
        console.log('encodedSecondInputForExecuteTransaction')
        console.log(encodedSecondInputForExecuteTransaction)

        //console.log(ethers.hexlify(Number(2816)))

        // let executeParams = {
        //   commands: 0x0b00,
        //   inputs: [
        //     encodedFirstInputForExecuteTransaction,
        //     encodedSecondInputForExecuteTransaction
        //   ],
        //   deadline: Math.floor(Date.now() / 1000) + 60 * 10,
        // }

        // Command Types where value<0x08, executed in the first nested-if block
        //V3_SWAP_EXACT_IN = 0x00;
        //V3_SWAP_EXACT_OUT = 0x01;
        //PERMIT2_TRANSFER_FROM = 0x02;
        //PERMIT2_PERMIT_BATCH = 0x03;
        //SWEEP = 0x04;
        //TRANSFER = 0x05;
        //PAY_PORTION = 0x06;
        //COMMAND_PLACEHOLDER_0x07 = 0x07;

        // Command Types where 0x08<=value<=0x0f, executed in the second nested-if block
        //V2_SWAP_EXACT_IN = 0x08;
        //V2_SWAP_EXACT_OUT = 0x09;
        //PERMIT2_PERMIT = 0x0a;
        //WRAP_ETH = 0x0b;
        //UNWRAP_WETH = 0x0c;
        //PERMIT2_TRANSFER_FROM_BATCH = 0x0d;
        //COMMAND_PLACEHOLDER_0x0e = 0x0e;
        //COMMAND_PLACEHOLDER_0x0f = 0x0f;

        let commands = '0x00' //V3_SWAP_EXACT_IN
        let inputs = [encodedFirstInputForExecuteTransaction]

        if (swapFromNativeETH === true) {
            commands = '0x0b00'
            inputs.push(encodedSecondInputForExecuteTransaction)
        } else if (swapToNativeETH === true) {
            commands = '0x000c'
            inputs.push(encodedSecondInputForExecuteTransaction)
        }

        console.log('data for EXECUTE transaction')
        console.log([
            commands,
            inputs,
            Math.floor(Date.now() / 1000) + 60 * 20
        ])

        let data = swapRouterContract.interface.encodeFunctionData('execute', [
            commands,
            inputs,
            Math.floor(Date.now() / 1000) + 60 * 20
        ])

        const executeTransaction = {
            to: swapRouterAddress,
            from: await wallet.getAddress(),
            //data: executeParams,
            data: data,
        };
        // const executeTransaction = {
        //   to: await wallet.getAddress(),
        //   //from: swapRouterAddress,
        //   //data: executeParams,
        //   data: data,
        //   //value: amountIn
        // };

        if (swapFromNativeETH === true) {
            executeTransaction.value = amountIn
        }

        console.log('=====================THE EXECUTE TRANSACTION IS===================')
        console.log(executeTransaction)


        // let exactInputParams = {
        //   path: encodedPathForExecuteTransaction,
        //   recipient: await wallet.getAddress(),
        //   deadline: Math.floor(Date.now() / 1000) + 60 * 20,
        //   amountIn: amountIn,
        //   amountOutMinimum: quote
        // }
        //
        // const exactInputTransaction = {
        //   to: swapRouterAddress,
        //   from: await wallet.getAddress(),
        //   data: swapRouterContract.interface.encodeFunctionData('exactInput', [exactInputParams]),
        //   value: amountIn
        // };
        //
        // console.log('=====================THE EXACT INPUT TRANSACTION IS===================')
        // console.log(exactInputTransaction)


        //ESTIMATE GAS
        await wallet.estimateGas(executeTransaction).then(async estimatedGas => {
            console.log('============ GAS AMOUNT ===========', estimatedGas)

            //let maxFeePerGas = await provider.getBlock("latest")
            let feeData = await provider.getFeeData()
            console.log('feeData')
            console.log(feeData)

            const multipliedGas = (feeData.maxFeePerGas+feeData.maxPriorityFeePerGas)*estimatedGas
            console.log(multipliedGas)

            const multipliedGasInEther = parseFloat(ethers.formatEther(multipliedGas));
            console.log('MULTIPLIED GAS IN ETH: ', multipliedGasInEther)

            //zzzzzzzzzzzzzzzzzzzzzzzz SLEEP zzzzzzzzzzzzzzzzzzzzzzzz//
            async function sleep(milliseconds) {
                const date = Date.now();
                let currentDate = null;
                do {
                    currentDate = Date.now();
                } while (currentDate - date < milliseconds);
            }
            console.log("Start sleep");
            await sleep(2000);
            console.log("End sleep!");
            //zzzzzzzzzzzzzzzzzzzzzzzz SLEEP zzzzzzzzzzzzzzzzzzzzzzzz//

            console.log('===================================================================')
            console.log('BEFORE transaction')
            console.log('swapFromNativeETH: ', swapFromNativeETH)
            console.log('multipliedGasInEther ', multipliedGasInEther)
            console.log('AMOUNT ', AMOUNT)
            console.log('multipliedGasInEther+AMOUNT ', Number(multipliedGasInEther)+AMOUNT)
            console.log('===================================================================')

            if ((swapFromNativeETH && Number(multipliedGasInEther)+AMOUNT <= balanceInEther) ||
                (!swapFromNativeETH && multipliedGasInEther <= balanceInEther)) {
                const executeTransactionResponse = await wallet.sendTransaction({
                    ...executeTransaction,
                    //gasPrice,
                    gasLimit: estimatedGas,
                    //maxPriorityFeePerGas: ethers.parseUnits("1.5", "gwei"),
                    maxPriorityFeePerGas: feeData.maxPriorityFeePerGas,
                    maxFeePerGas: feeData.maxFeePerGas,
                    //maxFeePerGas:
                    //value: 0,
                }).then(async (transaction) => {
                    console.log('Wallet balances After:')
                    await getWalletBalances(provider, wallet.getAddress(), tokenFromContract, tokenToContract)
                    console.log('====================================')
                    console.log(transaction)
                }).catch((err) => {
                    console.log('Failed send transaction')
                    console.log(err)
                });
                //TODO REFUND ETH
                //swapRouterContract.refundETH()
            } else {
                console.log('TRANSACTION CANCELLED: conditions have not been met')
                console.log('swapFromNativeETH: ', swapFromNativeETH)
                console.log('multipliedGasInEther ', multipliedGasInEther)
                console.log('AMOUNT ', AMOUNT)
                console.log('multipliedGasInEther+AMOUNT ', Number(multipliedGasInEther)+AMOUNT)
            }


        }).catch((err) => {
            console.log('FAILED ESTIMATE GAS')
            console.log(err)
        });
        //========================== EXECUTE TRANSACTION ==============================


        //=========================ENCODE PATH=========================
        //
        // let path = await encodePath([immutables.token1, immutables.token1, immutables.token0], [3000, 3000]);
        //
        // let exactInputParams = {
        //   path: path,
        //   recipient: await wallet.getAddress(),
        //   deadline: Math.floor(Date.now() / 1000) + 60 * 20,
        //   amountIn: amountIn,
        //   amountOutMinimum: 0
        // }
        //
        // console.log(exactInputParams)
        // //=========================ENCODE PATH=========================


// =============== EXACT_INPUT ==================
//
// TO CHECK GASLIMIT
//   const transaction = {
//     to: swapRouterAddress,
//     from: await wallet.getAddress(),
//     data: swapRouterContract.interface.encodeFunctionData('exactInputSingle', [newParams]),
//     value: amountIn
//   };
//
//   console.log('=====================THE TRANSACTION IS===================')
//   console.log(transaction)
//
//
//
//   const trans = {
//     to: swapRouterAddress,
//     from: await wallet.getAddress(),
//     data: swapRouterContract.interface.encodeFunctionData('exactInput', [exactInputParams])
//     //value: amountIn
//   };
//
//   console.log(trans)
//
//
//   await wallet.estimateGas(trans).then(async res => {
//     console.log('============ GAS AMOUNT ===========', res)
//
//     async function sleep(milliseconds) {
//       const date = Date.now();
//       let currentDate = null;
//       do {
//         currentDate = Date.now();
//       } while (currentDate - date < milliseconds);
//     }
//
//     console.log("Start sleep");
//     await sleep(2000);
//     console.log("End sleep!");
//
//
//     const transactionResponse = await wallet.sendTransaction({
//       ...trans,
//       //gasPrice,
//       gasLimit: res,
//       value: amountIn,
//     }).then(async (transaction) => {
//       console.log('Wallet balances After:')
//       await getWalletBalances(provider, wallet.getAddress(), tokenFromContract, tokenToContract)
//
//       console.log('====================================')
//
//       console.log(transaction)
//     }).catch((err) => {
//       console.log('Failed send transaction')
//       console.log(err)
//     });
//
//     //const receipt = await transactionResponse.wait()
//     //console. log ('receipt', receipt)
//
//   }).catch((err) => {
//     console.log('Failed send transaction')
//     console.log(err)
//   });
//   =============== EXACT_INPUT ==================


// ======================== MAIN TRANSACTION ================================= //
        // let newParams = {
        //   tokenIn: immutables.token1,
        //   tokenOut: immutables.token0,
        //   fee: immutables.fee,
        //   recipient: await wallet.getAddress(),
        //   deadline: Math.floor(Date.now() / 1000) + 60 * 10,
        //   amountIn: amountIn,
        //   amountOutMinimum: quote,
        //   sqrtPriceLimitX96: 0,
        //   value: 0,
        //   //maxPriorityFeePerGas: 5,
        //   nonce: await wallet.getNonce(),
        //   slippageTolerance: Number(constantsInput.slippage)
        // }
//   const transaction = {
//     to: swapRouterAddress,
//     from: await wallet.getAddress(),
//     data: swapRouterContract.interface.encodeFunctionData('exactInputSingle', [newParams]),
//     value: amountIn
//   };
//
//   console.log('=====================THE TRANSACTION IS===================')
//   console.log(transaction)
//
//
//   //const gasPrice = await provider.getGasPrice();
//   await wallet.estimateGas(transaction).then(async res => {
//     console.log('============ GAS AMOUNT ===========', res)
//
//     async function sleep(milliseconds) {
//       const date = Date.now();
//       let currentDate = null;
//       do {
//         currentDate = Date.now();
//       } while (currentDate - date < milliseconds);
//     }
//
//     console.log("Start sleep");
//     await sleep(2000);
//     console.log("End sleep!");
//
//
//     const transactionResponse = await wallet.sendTransaction({
//       ...transaction,
//       //gasPrice,
//       gasLimit: res,
//       //value: 0,
//     }).then(async (transaction) => {
//       console.log('Wallet balances After:')
//       await getWalletBalances(provider, wallet.getAddress(), tokenFromContract, tokenToContract)
//
//       console.log('====================================')
//
//       console.log(transaction)
//     }).catch((err) => {
//       console.log('Failed send transaction')
//       console.log(err)
//     });
//
//     //TODO REDFUND ETH
//     //swapRouterContract.refundETH()
//
//   }).catch((err) => {
//     console.log('Failed send transaction')
//     console.log(err)
//   });
// ======================== MAIN TRANSACTION ================================= //

        //let tripleGas = gasAmount*BigInt(3)
        //console.log('tripleGas ', tripleGas)

        /*const transaction = await swapRouterContract
          .connect(connectedWallet)
          .exactInputSingle(params, {
            //gasLimit: 2000000n ,
            //gasLimit: ethers.parseUnits(tripleGas.toString(), 18),
            gasLimit: tripleGas,
            from: wallet.getAddress(),
            value: amountIn
          })
          .then(async (transaction) => {
            console.log('Wallet balances After:')
            await getWalletBalances(provider, wallet.getAddress())

            console.log('====================================')

            console.log(transaction)
          })
          .catch((err) => {
            console.log('Failed send transaction')
            console.log(err)
          })*/

    } else {
        console.log('BALANCE IS LOWER THAN AMOUNT IN: ', `ETH balance: ${balanceInEther}, balance: ${tokenFromBalance}, amountIn: ${AMOUNT}, swap from native: ${swapFromNativeETH}`)
    }
}

const setTokensData = async () => {
    try {
        chainId = Number(constantsInput.chain_id)
        WALLET_PRIVATE_KEY = constantsInput.wallet_private_key

        let nativeToken

        if (!constantsInput.token_in || !constantsInput.token_out) {
            nativeToken = getNativeToken(chainId)
        }


        let tokenIn,
            tokenOut

        if (constantsInput.token_in) {
            tokenIn = constantsInput.token_in
        } else {
            tokenIn = nativeToken
            swapFromNativeETH = true
        }

        if (constantsInput.token_out) {
            tokenOut = constantsInput.token_out
        } else {
            tokenOut = nativeToken
            swapToNativeETH = true
        }

        TokenFrom.address = tokenIn
        tokenFromContract = new ethers.Contract(
            TokenFrom.address,
            ERC20ABI,
            //MATIC_ABI,
            provider
        )
        TokenFrom.symbol = await tokenFromContract.symbol()
        TokenFrom.name = await tokenFromContract.name()
        TokenFrom.decimals = await tokenFromContract.decimals()

        TOKEN_FROM = new Token(
            chainId,
            TokenFrom.address,
            Number(TokenFrom.decimals),
            TokenFrom.symbol,
            TokenFrom.name
        )


        TokenTo.address = tokenOut
        tokenToContract = new ethers.Contract(
            TokenTo.address,
            ERC20ABI,
            //ZETA_ABI,
            provider
        )
        TokenTo.symbol = await tokenToContract.symbol()
        TokenTo.name = await tokenToContract.name()
        TokenTo.decimals = await tokenToContract.decimals()

        TOKEN_TO = new Token(
            chainId,
            TokenTo.address,
            Number(TokenTo.decimals),
            TokenTo.symbol,
            TokenTo.name
        )

        console.log(TokenFrom)
        console.log(TokenTo)

        AMOUNT = Number(constantsInput.amount_in)

        tokens = {
            in: TOKEN_FROM,
            amountIn: constantsInput.amount_in,
            out: TOKEN_TO,
            poolFee: FeeAmount.MEDIUM
        }

        console.log('tokens')
        console.log(tokens)

        // immutables = {
        //   token0: TOKEN_TO.address,
        //   token1: TOKEN_FROM.address,
        //   fee: FeeAmount.MEDIUM
        // }
    } catch (e) {
        console.log('error in setTokensData')
        console.log(e)
        throw e
    }
}

const getNativeToken = (chainId) => {
    return tokensPreset[chainId].address
}

main()
